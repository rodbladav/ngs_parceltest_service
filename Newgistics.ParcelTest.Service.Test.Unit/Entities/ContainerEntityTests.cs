﻿using Newgistics.ParcelTest.Service.Test.Unit.Factories;
using Newgistics.ParcelTest.Service.Entities;
using Shouldly;
using Xunit;

namespace Newgistics.ParcelTest.Service.Test.Unit.Entities
{
    public class ContainerEntityTests
    {
        protected ContainerFactory ContainerFactory { get; set; }

        [Fact]
        public void GetSet_ContainerEntity()
        {
            ContainerFactory = new ContainerFactory();
            var container = ContainerFactory.GetContainerEntity();

            container.TransactionId.ShouldBe(container.TransactionId);
            container.Barcode.PrimaryBarcode.ShouldBe(container.Barcode.PrimaryBarcode);
            container.Barcode.Barcode2.ShouldBe(container.Barcode.Barcode2);
            container.Barcode.PostalBarcode.ShouldBe(container.Barcode.PostalBarcode);
            container.Barcode.PostalBarcodeBase.ShouldBe(container.Barcode.PostalBarcodeBase);
            container.Barcode.ClientAuxiliary.ShouldBe(container.Barcode.ClientAuxiliary);

            container.Dimension.Weight.ShouldBe(container.Dimension.Weight);
            container.Dimension.Length.ShouldBe(container.Dimension.Length);
            container.Dimension.Height.ShouldBe(container.Dimension.Height);
            container.Dimension.Width.ShouldBe(container.Dimension.Width);

            container.Facility.FacilityName.ShouldBe(container.Facility.FacilityName);
            container.Facility.FastTrakSiteId.ShouldBe(container.Facility.FastTrakSiteId);
            container.Facility.FacilityInductionDate.ShouldBe(container.Facility.FacilityInductionDate);
            container.Facility.SortCode.ShouldBe(container.Facility.SortCode);
            container.Facility.InductedFlag.ShouldBe(container.Facility.InductedFlag);
        }
    }
}
