﻿using Newgistics.ParcelTest.Service.Repositories;
using Newgistics.ParcelTest.Service.Entities;
using Newgistics.ParcelTest.Service.Models;
using Newgistics.ParcelTest.Service.Test.Unit.Factories;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Linq;
using Shouldly;
using System;
using Xunit;
using Moq;
using Newgistics.Lib.MongoDb.Interfaces;

namespace Newgistics.ParcelTest.Service.Test.Unit.Repositories
{
    public class ContainerRepositoryTests
    {
        protected ContainerFactory ContainerFactory { get; set; }

        [Fact]
        public async void GetByTransactionId_NotFound()
        {
            ContainerFactory = new ContainerFactory();
            var containerEntityList = ContainerFactory.GetContainerEntityList();
            var dataStore = new Mock<IRepository<ContainerEntity>>();

            dataStore.Setup(d => d.FindManyAsync(It.IsAny<Expression<Func<ContainerEntity, bool>>>()))
                .Returns((Expression<Func<ContainerEntity, bool>> predicate) =>
                    Task.FromResult(containerEntityList.Where(predicate.Compile())));

            var containerRepository = new ContainerRepository(dataStore.Object, new MemoryCache(new MemoryCacheOptions()));
            var result = await containerRepository.GetByTransactionId("0000");

            result.Count.ShouldBe(0);
        }

        [Fact]
        public async void GetByTransactionId_Found()
        {
            ContainerFactory = new ContainerFactory();
            var containerEntityList = ContainerFactory.GetContainerEntityList();
            var dataStore = new Mock<IRepository<ContainerEntity>>();

            dataStore.Setup(d => d.FindManyAsync(It.IsAny<Expression<Func<ContainerEntity, bool>>>()))
                .Returns((Expression<Func<ContainerEntity, bool>> predicate) =>
                    Task.FromResult(containerEntityList.Where(predicate.Compile())));

            var containerRepository = new ContainerRepository(dataStore.Object, new MemoryCache(new MemoryCacheOptions()));
            var result = await containerRepository.GetByTransactionId("1000");

            result.Count.ShouldBe(1);
        }
    }
}
