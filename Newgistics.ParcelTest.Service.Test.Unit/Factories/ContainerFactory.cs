using Newgistics.ParcelTest.Service.Models;
using Newgistics.ParcelTest.Service.Entities;
using System.Collections.Generic;

namespace Newgistics.ParcelTest.Service.Test.Unit.Factories
{
    public class ContainerFactory
    { 
        public Container GetContainer()
        {
            return new Container
            {
                TransactionId = "1000",
                Barcode = new Barcode
                {
                    PrimaryBarcode = "4208751392748927005143010055122175",
                    Barcode2 = "4208751392748927005143010055122175",
                    PostalBarcode = "4208751392748927005143010055122175",
                    PostalBarcodeBase = "92748927005143010055122175",
                    ClientAuxiliary = "4208751392748927005143010055122175"
                },
                Dimension = new Dimension
                {
                    Weight = 0.5020,
                    Height = 4.0,
                    Length = 8.0,
                    Width = 1.0
                },
                Facility = new Facility
                {
                    FacilityName = "DFW4",
                    FastTrakSiteId = 25,
                    FacilityInductionDate = System.DateTime.UtcNow,
                    SortCode = "GJAUS241",
                    InductedFlag = true
                }
            };
        }

        public ContainerEntity GetContainerEntity()
        {
            return new ContainerEntity
            {
                TransactionId = "1000",
                Barcode = new BarcodeEntity
                {
                    PrimaryBarcode = "4208751392748927005143010055122175",
                    Barcode2 = "4208751392748927005143010055122175",
                    PostalBarcode = "4208751392748927005143010055122175",
                    PostalBarcodeBase = "92748927005143010055122175",
                    ClientAuxiliary = "4208751392748927005143010055122175"
                },
                Dimension = new DimensionEntity
                {
                    Weight = 0.5020,
                    Height = 4.0,
                    Length = 8.0,
                    Width = 1.0
                },
                Facility = new FacilityEntity
                {
                    FacilityName = "DFW4",
                    FastTrakSiteId = 25,
                    FacilityInductionDate = System.DateTime.UtcNow,
                    SortCode = "GJAUS241",
                    InductedFlag = true
                }
            };
        }

        public List<ContainerEntity> GetContainerEntityList()
        {
            var containerEntityList = new List<ContainerEntity>();
            for (var i=0; i<1; i++)
            {
                containerEntityList.Add(GetContainerEntity());
            }
            return containerEntityList;
        }
    }
}
