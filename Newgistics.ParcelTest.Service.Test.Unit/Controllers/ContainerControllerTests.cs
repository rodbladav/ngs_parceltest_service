﻿using Newgistics.ParcelTest.Service.Repositories.Interfaces;
using Newgistics.ParcelTest.Service.Test.Unit.Factories;
using Newgistics.ParcelTest.Service.Controllers;
using Newgistics.ParcelTest.Service.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Shouldly;
using Xunit;
using Moq;
using Newgistics.Lib.ServiceHost.Model;
using Newgistics.Lib.ServiceHost.Helper;

namespace Newgistics.ParcelTest.Service.Test.Unit.Controllers
{
    public class ContainerControllerTests
    {
        protected ContainerFactory ContainerFactory { get; set; }

        [Fact]
        public async void CreateUpdateContainer_ReturnOkResult()
        {
            ContainerFactory = new ContainerFactory();
            var container = ContainerFactory.GetContainer();

            var containerRepository = new Mock<IContainerRepository>();
            containerRepository.Setup(r => r.CreateUpdateContainer(It.IsAny<Container>())).Returns(Task.CompletedTask);

            var containerController = new Mock<ContainerController>(containerRepository.Object);

            var result = await containerController.Object.CreateUpdateContainer(container);

            result.ShouldNotBeNull();
            result.ShouldBeAssignableTo(typeof(OkResult));
            containerRepository.Verify(cr => cr.CreateUpdateContainer(It.IsAny<Container>()), Times.Once);
        }
    }
}
