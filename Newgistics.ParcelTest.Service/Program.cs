﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Newgistics.Lib.ServiceHost.Helper;

namespace Newgistics.ParcelTest.Service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IWebHost webHost = WebHostHelper.GetHost(typeof(Startup), args);
            webHost.Run();
        }
    }
}