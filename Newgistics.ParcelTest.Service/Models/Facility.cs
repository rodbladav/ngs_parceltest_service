﻿using System;

namespace Newgistics.ParcelTest.Service.Models
{
    public class Facility
    {
        public string FacilityName { get; set; }

        public int FastTrakSiteId { get; set; }

        public DateTime? FacilityInductionDate { get; set; }

        public string SortCode { get; set; }

        public bool InductedFlag { get; set; }
    }
}