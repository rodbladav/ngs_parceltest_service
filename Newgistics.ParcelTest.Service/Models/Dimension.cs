﻿using System;

namespace Newgistics.ParcelTest.Service.Models
{
    public class Dimension
    {
        public double Weight { get; set; }

        public double Height { get; set; }

        public double Length { get; set; }

        public double Width { get; set; }
    }
}