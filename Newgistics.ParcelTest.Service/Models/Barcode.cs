﻿using System;

namespace Newgistics.ParcelTest.Service.Models
{
    public class Barcode
    {
        public string PrimaryBarcode { get; set; }

        public string Barcode2 { get; set; }

        public string PostalBarcode { get; set; }

        public string PostalBarcodeBase { get; set; }

        public string ClientAuxiliary { get; set; }
    }
}