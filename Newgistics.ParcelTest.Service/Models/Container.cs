﻿using System.ComponentModel.DataAnnotations;

namespace Newgistics.ParcelTest.Service.Models
{    public class Container
    {
        [Required]
        public string TransactionId { get; set; }

        public Barcode Barcode { get; set; }

        public Dimension Dimension { get; set; }

        public Facility Facility { get; set; }
    }
}