﻿using System;
using Newgistics.ParcelTest.Service.Repositories.Interfaces;
using Newgistics.ParcelTest.Service.Repositories;
using Newgistics.ParcelTest.Service.Entities;
using Microsoft.Extensions.DependencyInjection;
using Newgistics.ParcelTest.Service.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newgistics.Lib.MongoDb.Extensions;
using Newgistics.Lib.MongoDb.Interfaces;
using Newgistics.Lib.ServiceHost.Middleware;
using Swashbuckle.AspNetCore.Swagger;
using Newgistics.Lib.MongoServiceHost;

namespace Newgistics.ParcelTest.Service
{
    public class Startup
    {
        private readonly IHostingEnvironment _environment;
        private readonly IConfiguration _configuration;

        public Startup(IHostingEnvironment env, IConfiguration config)
        {
            _environment = env;
            _configuration = config;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = _configuration.GetConnectionString("ContainerDb");

            services.AddMemoryCache();
            services.AddApiSupport(_configuration, _environment);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Parcel Test Service", Version = "v1" });
            });
            services.AddOptions();
            services.AddSingleton<IContainerRepository, ContainerRepository>();
            services.AddMongoDB<ContainerEntity>(connectionString);

            services.AddMongoHealthCheck(connectionString);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime appLifetime)
        {
            app.UseStaticFiles();
            app.UseApiSupport(env, loggerFactory, appLifetime);
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Parcel Test Service");
            });
         }
    }
}