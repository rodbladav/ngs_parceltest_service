﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace Newgistics.ParcelTest.Service.Entities
{
    [BsonIgnoreExtraElements]
    public class FacilityEntity
    {
        public string FacilityName { get; set; }

        public int FastTrakSiteId { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime FacilityInductionDate { get; set; }

        public string SortCode { get; set; }

        public bool InductedFlag { get; set; }
    }
}
