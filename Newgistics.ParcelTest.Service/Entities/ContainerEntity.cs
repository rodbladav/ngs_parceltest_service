﻿using MongoDB.Bson.Serialization.Attributes;
using Newgistics.Lib.MongoDb.Attributes;
using MongoDB.Bson;

namespace Newgistics.ParcelTest.Service.Entities
{
    [BsonIgnoreExtraElements]
    public class ContainerEntity
    {
        [BsonId]
        [BsonIgnoreIfNull]
        public ObjectId? ContainerId { get; set; }

        [SingleIndex]
        [GroupIndex("Idx1")]
        public string TransactionId { get; set; }

        public BarcodeEntity Barcode { get; set; }

        public DimensionEntity Dimension { get; set; }

        public FacilityEntity Facility { get; set; }
    }
}
