﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace Newgistics.ParcelTest.Service.Entities
{
    [BsonIgnoreExtraElements]
    public class BarcodeEntity
    {
        public string PrimaryBarcode { get; set; }

        public string Barcode2 { get; set; }

        public string PostalBarcode { get; set; }

        public string PostalBarcodeBase { get; set; }

        public string ClientAuxiliary { get; set; }
    }
}
