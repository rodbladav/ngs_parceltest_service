﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace Newgistics.ParcelTest.Service.Entities
{
    [BsonIgnoreExtraElements]
    public class DimensionEntity
    {
        public double Weight { get; set; }

        public double Height { get; set; }

        public double Length { get; set; }

        public double Width { get; set; }
    }
}
