﻿using Newgistics.ParcelTest.Service.Repositories.Interfaces;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newgistics.ParcelTest.Service.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Newgistics.Lib.ServiceHost.Controller;
using Newgistics.Lib.ServiceHost.Helper;
using System.Collections.Generic;

namespace Newgistics.ParcelTest.Service.Controllers
{
    [Produces("application/json")]
    public class ContainerController : BaseController
    {
        private readonly IContainerRepository _containerRepository;

        public ContainerController(IContainerRepository containerRepository)
        {
            _containerRepository = containerRepository;
        }

        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(RestOkResponse<Container>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError)]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden)]
        [SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        [HttpGet("/v1/container/transactionid/{transactionId}")]
        public async Task<IActionResult> GetByTransactionId(string transactionId)
        {
            var container = await _containerRepository.GetByTransactionId(transactionId);

            if (container.Count < 1)
            {
                return NotFound();
            }
            return Ok(container);
        }

        [SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(RestErrorResponse))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError)]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden)]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK)]
        [HttpPut("v1/container")]
        public async Task<IActionResult> CreateUpdateContainer([FromBody]Container container)
        {
            await _containerRepository.CreateUpdateContainer(container);
            return new OkResult();
        }

        [SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(RestErrorResponse))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError)]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden)]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK)]
        [HttpPut("v1/container/multi")]
        public async Task<IActionResult> CreateMultipleContainers([FromBody]List<Container> containers)
        {
            await _containerRepository.CreateMultipleContainers(containers);
            return new OkResult();
        }
    }
}