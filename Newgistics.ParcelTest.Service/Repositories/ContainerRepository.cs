﻿using Newgistics.ParcelTest.Service.Repositories.Interfaces;
using Newgistics.ParcelTest.Service.Entities;
using Newgistics.ParcelTest.Service.Models;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Linq;
using AutoMapper;
using System;
using Newgistics.Lib.MongoDb.Interfaces;

namespace Newgistics.ParcelTest.Service.Repositories
{
    public class ContainerRepository : IContainerRepository
    {
        private readonly IRepository<ContainerEntity> _dataStore;
        private readonly IMemoryCache _memoryCache;
        private readonly IMapper _entityMapper;

        public ContainerRepository(IRepository<ContainerEntity> dataStore, IMemoryCache memoryCache)
        {
            _dataStore = dataStore;
            _memoryCache = memoryCache;

            var entityToModelConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Container, ContainerEntity>();
            });

            _entityMapper = entityToModelConfiguration.CreateMapper();
        }

        public async Task<List<Container>> GetByTransactionId (string transactionId)
        {
            var container = await _dataStore.FindManyAsync(x => x.TransactionId == transactionId);

            return container?.Select(_entityMapper.Map<Container>).ToList();
        }

        public async Task CreateUpdateContainer(Container container)
        {
            var containerEntity = _entityMapper.Map<ContainerEntity>(container);
            containerEntity.ContainerId = null;
            await _dataStore.FindOneAndReplaceUpsertAsync(x => x.TransactionId == container.TransactionId, containerEntity);
        }

        public async Task CreateMultipleContainers(List<Container> containers)
        {
            var containerEntities = _entityMapper.Map<List<ContainerEntity>>(containers);
            containerEntities.ForEach(c => c.ContainerId = null);
            await _dataStore.AddManyAsync(containerEntities);
        }
    }
}
