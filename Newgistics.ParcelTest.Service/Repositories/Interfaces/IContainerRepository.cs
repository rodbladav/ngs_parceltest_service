﻿using Newgistics.ParcelTest.Service.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Newgistics.ParcelTest.Service.Repositories.Interfaces
{
    public interface IContainerRepository
    {
        Task<List<Container>> GetByTransactionId(string transactionId);

        Task CreateUpdateContainer(Container container);

        Task CreateMultipleContainers(List<Container> containers);
    }
}
